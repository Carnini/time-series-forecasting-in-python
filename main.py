"""From data to model"""
import numpy as np

from prepare_data import get_mape
from prepare_data import make_plot
from prepare_data import plot_february
from prepare_data import prepare_data
from prepare_data import split_train_test
from train_models import train_exponential_smoothing
from train_models import train_ols

if __name__ == "__main__":
    df = prepare_data(
        "raw_input/hr_temp_20170201-20200131_subset.csv",
        "raw_input/hrl_load_metered - 20170201-20200131.csv",
    )
    train, test = split_train_test("2020-01-01", df)
    print(train.shape)
    print(test.shape)

    ols = train_ols(train)
    es = train_exponential_smoothing(
        np.array(train["mw"]) - np.array(ols.predict(train))
    )

    Y = np.array(train["mw"])
    Y_hat = np.array(ols.predict(train))
    Y_hat_prime = np.array(es.fittedvalues)
    print("Training until January")
    print(get_mape(Y, Y_hat))
    print(get_mape(Y, Y_hat + Y_hat_prime))

    Y_hat = np.array(ols.predict(test))
    Y_hat_prime = np.array(es.forecast(744))
    make_plot(test, Y_hat + Y_hat_prime, "january_prediction.png")

    train, test = split_train_test("2020-02-01", df)
    ols = train_ols(train)
    es = train_exponential_smoothing(
        np.array(train["mw"]) - np.array(ols.predict(train))
    )
    Y = np.array(train["mw"])
    Y_hat = np.array(ols.predict(train))
    Y_hat_prime = np.array(es.fittedvalues)
    print("Retraining with January")
    print(get_mape(Y, Y_hat))
    print(get_mape(Y, Y_hat + Y_hat_prime))

    new_data = prepare_data(
        "raw_input/hr_temp_20200201-20200229_subset.csv",
        "raw_input/hrl_load_metered - 20170201-20200131.csv",
    )
    train, test = split_train_test("2020-02-01", new_data)
    print(train.shape)
    print(test.shape)
    Y_hat = np.array(ols.predict(test))
    Y_hat_prime = es.forecast(696)
    plot_february(test, Y_hat + Y_hat_prime, "february.png")
    print("Scored February")
