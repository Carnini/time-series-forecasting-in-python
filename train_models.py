"""Training models"""
import pandas as pd
from statsmodels.regression.linear_model import OLS
from statsmodels.tsa.holtwinters import ExponentialSmoothing
from statsmodels.tsa.holtwinters import HoltWintersResults


def train_ols(data: pd.DataFrame):
    """Training a linear model"""
    return OLS.from_formula(
        "mw ~ temp + C(day) + C(weekday) + C(month) + temp + temp * temp + temp * C(hour) + "
        + "temp * C(month) + temp * temp * C(hour) + temp * temp * C(month) +"
        + " C(hour) * C(weekday) + isHoliday",
        data=data,
    ).fit()


def train_exponential_smoothing(residual: pd.Series) -> HoltWintersResults:
    """Training an exponential smoothing"""
    return ExponentialSmoothing(
        residual,
        trend=None,
        seasonal="add",
        seasonal_periods=24).fit()
