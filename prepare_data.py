"""Preparing data and calculate features"""
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from pandas.tseries.holiday import USFederalHolidayCalendar


def prepare_data(temp_file: str, energy_file: str) -> pd.DataFrame():
    """Hardcoded input files, return a unique data frame with training and testing"""
    weather = pd.read_csv(
        temp_file,
        usecols=["DATE", "HourlyDryBulbTemperature"],
    ).rename(columns={"DATE": "date"})
    weather["date"] = pd.to_datetime(weather["date"], utc=True)

    energy = pd.read_csv(
        energy_file,
        usecols=["datetime_beginning_utc", "mw"],
    ).rename(columns={"datetime_beginning_utc": "date"})
    energy["date"] = pd.to_datetime(energy["date"])

    weather["date-hour"] = (
        weather["date"].dt.date.astype(str)
        + "_"
        + (weather["date"]).dt.hour.astype(str)
    )

    energy["date-hour"] = (
        energy["date"].dt.date.astype(str) + "_" + weather["date"].dt.hour.astype(str)
    )

    merged = (
        weather.merge(energy, how="left", on="date-hour")
        .drop(["date-hour", "date_y"], axis=1)
        .rename(columns={"date_x": "date", "HourlyDryBulbTemperature": "temp"})
        .sort_values("date")
    )

    merged["hour"] = (merged["date"]).dt.hour
    merged["day"] = (merged["date"]).dt.day
    merged["weekday"] = (merged["date"]).dt.weekday
    merged["month"] = (merged["date"]).dt.month
    merged["year"] = (merged["date"]).dt.year
    merged = merged[["date", "mw", "temp", "hour", "day", "weekday", "month", "year"]]

    merged.set_index("date")
    merged["mw"] = merged["mw"].interpolate()
    merged["temp"] = merged["temp"].interpolate()

    cal = USFederalHolidayCalendar()
    holidays = pd.Series(
        cal.holidays(start=merged["date"].iloc[0], end=merged["date"].iloc[-1])
    )

    date = (
        merged["date"].dt.year.astype(str)
        + "-"
        + merged["date"].dt.month.astype(str)
        + "-"
        + merged["date"].dt.day.astype(str)
    )

    date_holidays = (
        holidays.dt.year.astype(str)
        + "-"
        + holidays.dt.month.astype(str)
        + "-"
        + holidays.dt.day.astype(str)
    )

    merged["isHoliday"] = date.isin(date_holidays).astype(int)
    return merged


def split_train_test(date: str, data: pd.DataFrame) -> tuple:
    """Splitting train and test using the input date"""
    return (
        data[data["date"] < pd.to_datetime(date, utc=True)],
        data[data["date"] >= pd.to_datetime(date, utc=True)],
    )


def get_mape(Y: np.array, Y_hat: np.array) -> float:
    """Calculating the MAPE metric"""
    return 100 * np.mean(np.abs(Y - Y_hat) / Y)


def make_plot(data: pd.DataFrame, y_hat: np.array, filename: str) -> None:
    """Plotting for report"""
    matplotlib.rcParams.update({"font.size": 14})
    plt.figure(figsize=(16, 9))
    plt.xlabel("Time")
    plt.ylabel("Energy (MW)")
    plt.plot(data["date"], data["mw"], label="Actual", linewidth=3)
    plt.plot(data["date"], pd.Series(y_hat), label="Predicted", linewidth=3)
    plt.legend()
    plt.savefig(filename, dpi=600)


def plot_february(data: pd.DataFrame, y_hat: np.array, filename: str) -> None:
    """Plotting for report"""
    matplotlib.rcParams.update({"font.size": 14})
    plt.figure(figsize=(16, 9))
    plt.title("Predictions for February 2020")
    plt.xlabel("Time")
    ticks = pd.to_datetime(
        pd.Series(
            [
                "2020-02-01",
                "2020-02-05",
                "2020-02-10",
                "2020-02-15",
                "2020-02-20",
                "2020-02-25",
                "2020-02-29",
            ]
        )
    )
    plt.xticks(ticks, ticks.dt.date)
    plt.ylabel("Energy (MW)")
    plt.plot(data["date"], pd.Series(y_hat), linewidth=3)
    plt.savefig(filename, dpi=600)
